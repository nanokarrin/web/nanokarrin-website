# Strona główna NanoKarrin

## Setup

### Git i gitlab
Do udziału w projekcie potrzebujesz konta na gitlabie i skonfigurowanego gita na swoim komputerze. Jeśli nie masz dostępu do repozytorium napisz do mnie.  
Czysty git: https://git-scm.com/  
GitKraken dla ludzi wolących ładne interfejsy graficzne: https://www.gitkraken.com/  

### Node
Będziesz też potrzebować zainstalowanego lokalnie node'a, najlepiej najnowszego LTSa: https://nodejs.org/  
Oprócz tego wygodnie będzie też mieć zainstalowanego globalnie Gatsbiego: https://www.gatsbyjs.org/  
Po instalacji node'a można to zrobić poleceniem:
```
npm install -g gatsby-cli
```

### Klonowanie i inicjalizacja projektu
Po spełnieniu powyższych wymagań można sklonować repozytorium i zainstalować zależności:
```
git clone git@gitlab.com:nanokarrin/web/nanokarrin-website.git
npm install
```

## Development

### Fast development
Postawienia lokalnego serwera który sam się przeładowuje jak wykryje zmiany:
```
gatsby develop
```

### Wprowadzanie zmian
Wszystkie zmiany commitujemy na feature branche. Np. kiedy pracuję nad komponentem z listą konwentów mogę założyć brancha "feature-konwenty" i na niego wrzucać zmiany. Po zakończeniu pracy nad komponentem otwieram merge request w gitlabie i przypisuje osobę do jego rozpatrzenia.
