// set for local development without Vercel
process.env.CHANNEL_ID = 'UCVpYpPBN3LubTphRe-U2Tgg';
process.env.YOUTUBE_API_KEY = 'AIzaSyBYj3aad2DJ4DdBqr3nSSC6YetERpFHOmM';

module.exports = {
  siteMetadata: {
    title: "NanoKarrin",
    author: "NanoKarrin",
    description: "Polska Grupa Dubbingowa NanoKarrin"
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'NanoKarrin',
        short_name: 'NanoKarrin',
        start_url: '/',
        background_color: '#663399',
        theme_color: '#663399',
        display: 'minimal-ui',
        icon: 'src/assets/images/website-icon.png',
      },
    },
    {
      resolve: `gatsby-source-youtube`,
      options: {
        channelId: [process.env.CHANNEL_ID],
        apiKey: process.env.YOUTUBE_API_KEY,
        maxVideos: 500
      },
    },
    'gatsby-plugin-sass',
    'gatsby-plugin-offline'
  ],
};
