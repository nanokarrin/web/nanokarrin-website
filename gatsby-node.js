/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

const path = require(`path`);

exports.createPages = async ({ graphql, actions }) => {
    const { createPage } = actions;
    const result = await graphql(`
    query {
      allYoutubeVideo {
        nodes {
        videoId
        publishedAt
        title   
        description
        }
      }
    }
  `);
    const videos = result.data.allYoutubeVideo.nodes;
    videos.forEach(v => {
        const page = {
            path: v.videoId,
            component: path.resolve(`./src/components/generic.js`),
            context: {
                ...v
            }
        };
        console.log(page);
        createPage(page);
    });
};
