import React from 'react'
import cons from './cons'

const Conventions = () => {
    const conList = cons;
    const conCounter = 100;

    return (
        <section className="main accent2" id="conventions">
            <header className="major">
                <h2>Konwenty</h2>
                <p>Do tej pory byliśmy na <span className="con-counter">{conCounter}</span>
                konwentach!
                </p>
            </header>

            <div className="logo-grid">
                {conList.map(c => <div><img src={c.imgSrc} alt={c.alt} /></div>)}
            </div>

        </section>
    );
}

export default Conventions;
