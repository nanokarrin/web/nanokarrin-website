import React from 'react'

const Features = () => (
    <section className="main" id="features">

        <header className="major">
            <h2>Znajdź nas</h2>
        </header>
        <div className="features">
            <section>
                <a className="icon solid brands fa-youtube fa-5x accent3" href="https://www.youtube.com/channel/UCue2Utve3Cz8Cb2eIJzWGUQ" target="_blank"/>
                <h3><a href="https://www.youtube.com/channel/UCue2Utve3Cz8Cb2eIJzWGUQ" target="_blank">YouTube</a></h3>
                <p>Tu znajdziesz nasze projekty.</p>
            </section>
            <section>
                <a className="icon solid brands fa-discord fa-5x accent2" href="https://discord.gg/nanokarrin" target="_blank"/>
                <h3><a href="https://discord.gg/nanokarrin" target="_blank">Discord</a></h3>
                <p>Tu z nami porozmawiasz.</p>
            </section>
            <section>
                <a className="icon solid fa-music fa-5x accent4" href="https://mp3.nanokarrin.pl" target="_blank"/>
                <h3><a href="https://mp3.nanokarrin.pl" target="_blank">Piosenkowo</a></h3>
                <p>Tu nas posłuchasz.</p>
            </section>
        </div>
    </section>
);

export default Features;
