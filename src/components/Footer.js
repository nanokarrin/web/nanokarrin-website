import { Link } from 'gatsby'
import React from 'react'

const Footer = props => (
    <footer id="footer">
      <section className="links">
        <div>
          <h3>Media społecznościowe</h3>
          <ul className="plain">
            <li><a href="#">Aliquam tempus</a></li>
            <li><a href="#">Ultrecies nul</a></li>
            <li><a href="#">Gravida ultricies</a></li>
            <li><a href="#">Commodo etiam</a></li>
          </ul>
        </div>
        <div>
          <h3>Nasze serwisy</h3>
          <ul className="plain">
            <li><a href="#">Morbi sem lorem</a></li>
            <li><a href="#">Praes sed dapi</a></li>
            <li><a href="#">Sed adipis nullam</a></li>
            <li><a href="#">Fus dolor lacinia</a></li>
          </ul>
        </div>
        <div>
          <h3>Partnerzy</h3>
          <ul className="plain">
            <li><a href="#">Donecnec etiam</a></li>
            <li><a href="#">Aapibus sedun</a></li>
            <li><a href="#">Namnulla tempus</a></li>
            <li><a href="#">Morbi set amet</a></li>
          </ul>
        </div>
        <div>
          <h3>Aliquam</h3>
          <ul className="plain">
            <li><a href="#">Lorem prasent dia</a></li>
            <li><a href="#">Nellentes ipsum</a></li>
            <li><a href="#">Diamsed arcu dolor</a></li>
            <li><a href="#">Sit amet cursus</a></li>
          </ul>
        </div>
      </section>
      <p className="copyright">&copy; NanoKarrin 2020</p>
    </footer>
);

export default Footer
