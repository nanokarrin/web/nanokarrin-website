import React from 'react'
import logo from '../assets/images/nk-logo-biale.png'
import {graphql, useStaticQuery} from "gatsby";

const Header = () => {
    const data = useStaticQuery(graphql`
    {
  allYoutubeVideo(limit: 1, sort: {fields: publishedAt, order: DESC}) {
    nodes {
      videoId
      publishedAt
    }
  }
}
    `);
    return (
    <div className="wrapper">
        <div className="inner">
            <header id="header">
                <img src={logo} style={{width: "40%"}} />
            </header>

            <section id="banner">
                <div className="items">
                    <section className="accent2">
                        <h1>Kontakt i media</h1>
                        <p></p>
                        <div className="contact-icons-wrapper">
                            <ul className="contact-icons">
                                <li><a className="fab fa-discord fa-3x" href="https://discord.gg/nanokarrin" target="_blank"/></li>
                                <li><a className="fab fa-facebook fa-3x" href="https://www.facebook.com/NanoKarrin/" target="_blank"/></li>
                                <li><a className="fab fa-instagram fa-3x" href="https://www.instagram.com/nanokarrin/" target="_blank"/></li>
                                <li><a className="fab fa-youtube fa-3x" href="https://www.youtube.com/channel/UCue2Utve3Cz8Cb2eIJzWGUQ" target="_blank"/></li>
                                {/*<li><a className="fab fa-spotify fa-3x" href="https://open.spotify.com/artist/3oNAGS1LrM1IKf0OZPoFwg" target="_blank"/></li>*/}
                            </ul>
                        </div>
                    </section>
                    <section className="accent3">
                        <h1>Dołącz do nas</h1>
                        <p style={{marginBottom: '1em'}}>Już dzisiaj zacznij <br /> swoją przygodę z dubbingiem!</p>
                        <ul className="actions special">
                            <li><a href="https://info.nanokarrin.pl/docs/szkolka0" className="button primary">Szkółka</a></li>
                            <li><a href="https://info.nanokarrin.pl/docs/rekruogolne" className="button primary">Rekrutacja</a></li>
                        </ul>
                    </section>
                </div>
                <div className="slider">
                    <article className="visible">
                        <iframe width="100%" height="100%" src={`https://www.youtube.com/embed/${data.allYoutubeVideo.nodes[0].videoId}`} frameBorder="0"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowFullScreen/>
                    </article>
                </div>
            </section>
        </div>
    </div>
);
}

export default Header
