import React from 'react'
import logo from "../assets/images/nk-logo-biale.png";
import { Link } from 'gatsby';

const HeaderGeneric = (props) => (
    <div className="wrapper">
        <div className="inner">

            <header id="header">
                <Link to={"/"}>
                    <img src={logo} style={{width: "40%"}} />
                </Link>
            </header>

        </div>
    </div>
)

export default HeaderGeneric
