import React, {useCallback, useState} from "react";
import Gallery from "react-photo-gallery";
import Carousel, {Modal, ModalGateway} from "react-images";
import {photos} from "./KarrinGalleryPhotos";

const KarrinGallery = () => {
	const [currentImage, setCurrentImage] = useState(0);
	const [viewerIsOpen, setViewerIsOpen] = useState(false);

	const openLightbox = useCallback((event, { photo, index }) => {
		setCurrentImage(index);
		setViewerIsOpen(true);
	}, []);

	const closeLightbox = () => {
		setCurrentImage(0);
		setViewerIsOpen(false);
	};
	return (
    <section className="main accent3" id="karrin-gallery">
        <header className="major">
            <h2>Karrin Galeria</h2>
        </header>
		<div>
		    <Gallery photos={photos} onClick={openLightbox} />
			<ModalGateway>
				{viewerIsOpen ? (
					<Modal onClose={closeLightbox}>
						<Carousel
								currentIndex={currentImage}
								views={photos.map(x => ({
								...x,
								srcset: x.srcSet
						}))}
						/>
					</Modal>
				) : null}
			</ModalGateway>
		</div>
    </section>
	);
}

export default KarrinGallery;
