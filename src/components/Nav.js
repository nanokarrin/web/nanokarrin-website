import React, {useEffect, useState} from 'react'
import Scrollspy from 'react-scrollspy'
import Scroll from './Scroll'

const Nav = ({stickyRef}) => {
    const [sticky, setSticky] = useState(false);

    useEffect(() => {
        window.addEventListener('scroll', pastNav);
        return () => window.removeEventListener('scroll', pastNav);
    }, []);

    function pastNav() {
        if (stickyRef.current) {
            setSticky(window.pageYOffset > stickyRef.current.offsetTop);
            console.log(`${window.pageYOffset} > ${stickyRef.current.offsetTop}`);
        } else {
            setSticky(false);
        }
    }

    return (
        <nav id="nav" className={sticky ? 'alt' : ''}>
            <Scrollspy items={ ['features', 'conventions', 'karrin-gallery', 'partners'] } currentClassName="is-active" offset={-20}>
                <li>
                    <Scroll type="id" element="features">
                        <a href="#">Znajdź nas</a>
                    </Scroll>
                </li>
                <li>
                    <Scroll type="id" element="conventions">
                        <a href="#">Konwenty</a>
                    </Scroll>
                </li>
                <li>
                    <Scroll type="id" element="karrin-gallery">
                        <a href="#">Karrin</a>
                    </Scroll>
                </li>
                <li>
                    <Scroll type="id" element="partners">
                        <a href="#">Współpraca</a>
                    </Scroll>
                </li>
            </Scrollspy>
        </nav>
    );
}

export default Nav
