import React from 'react'
import pic07 from "../assets/images/pic07.jpg";
import pic08 from "../assets/images/pic08.jpg";

const Partners = () => (
    <section className="main" id="partners">
        <header className="major">
            <h2>Współpraca i inicjatywy</h2>
            <p>Nasi partnerzy, sojusznicy i inicjatywy poza główną działalnością</p>
        </header>
        <div className="spotlights">
            <article>
                <div className="image"><img src={pic07} alt="" data-position="center"/></div>
                <div className="content">
                    <h3>Animagia</h3>
                    <p>Morbi in sem quis dui placerat ellentesque odio nisi euismod in haretra a, ultricies in, diam ed
                        rcuc consequat lorem ipsum dolor.</p>
                    <ul className="actions special">
                        <li><a href="#" className="button primary">More</a></li>
                    </ul>
                </div>
            </article>
            <article>
                <div className="image"><img src={pic08} alt="" data-position="center"/></div>
                <div className="content">
                    <h3>Bitwy Dubbingowe</h3>
                    <p>Eed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu
                        augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus.
                        Praesent elementum lorem ipsum blandid sti amet velvelit.</p>
                    <ul className="actions special">
                        <li><a href="#" className="button primary">More</a></li>
                    </ul>
                </div>
            </article>
            <article>
                <div className="image"><img src={pic08} alt="" data-position="center"/></div>
                <div className="content">
                    <h3>Wolne Dubbingowanie i warsztaty</h3>
                    <p>Eed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu
                        augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus.
                        Praesent elementum lorem ipsum blandid sti amet velvelit.</p>
                    <ul className="actions special">
                        <li><a href="#" className="button primary">More</a></li>
                    </ul>
                </div>
            </article>
        </div>
    </section>
);

export default Partners;
