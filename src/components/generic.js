import React from 'react'
import Helmet from 'react-helmet'

import Layout from '../components/layout'
import HeaderGeneric from '../components/HeaderGeneric'

import get from 'lodash/get';

const Generic = (data) => {
    const description = get(data, 'pageResources.json.pageContext.description');
    const title = get(data, 'pageResources.json.pageContext.title');
    const publishedAt = get(data, 'pageResources.json.pageContext.publishedAt');
    const videoId = get(data, 'pageResources.json.pageContext.videoId');
    return (
      <Layout>
        <Helmet title="Generic Page Title" />
        <HeaderGeneric />

          <div className="wrapper">
              <div className="inner">

                  <section className="main">

                      <header className="major">
                          <h1>{title}</h1>
                          <p>{new Date(publishedAt).toLocaleDateString()}</p>
                      </header>


                      <iframe width="100%" height="500px" src={`https://www.youtube.com/embed/${videoId}`} frameBorder="0"
                              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                              allowFullScreen/>

                      <hr/>
                      <h2>Opis</h2>
                      <p>{description}</p>

                      <hr/>

                  </section>

              </div>
          </div>
      </Layout>
    )
};

export default Generic
