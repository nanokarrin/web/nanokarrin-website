import React, { useState, useRef } from 'react'
import Helmet from 'react-helmet'
import {Waypoint} from 'react-waypoint'
import Header from '../components/Header'
import Layout from '../components/layout'
import KarrinGallery from "../components/KarrinGallery";
import Conventions from "../components/Conventions";
import Partners from "../components/Partners";
import Nav from "../components/Nav";
import Features from "../components/Features";

const Index = () => {
  const ref = useRef();

  return (
    <Layout>
      <Helmet title="NanoKarrin" />
      <Header />


      <div className="wrapper">
        <div className="inner">

          <section className="main" style={{padding: "0"}} ref={ref}>
            <Nav stickyRef={ref}/>
          </section>

          <Features/>

          <Conventions />

          <KarrinGallery />

          <section className="main accent1">
            <header className="major">
              <h2>Akcent 1</h2>
              <p>podtytuł</p>
            </header>
          </section>

          <section className="main accent2">
            <header className="major">
              <h2>Akcent 2</h2>
              <p>podtytuł</p>
            </header>
          </section>

          <section className="main accent3">
            <header className="major">
              <h2>Akcent 3</h2>
              <p>podtytuł</p>
            </header>
          </section>

          <section className="main accent4">
            <header className="major">
              <h2>Akcent 4</h2>
              <p>podtytuł</p>
            </header>
          </section>

          <Partners/>
        </div>
      </div>
    </Layout>
  );
};

export default Index
